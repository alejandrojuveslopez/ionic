import { Component } from '@angular/core';
import {Router, NavigationExtras} from '@angular/router'; 

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  disableInput:boolean=false;
  textForgot:string = "forgot your password"
  textUsername:string = "";
  textPass:string = "";
  colore:string="";
  colore2:string="";
  constructor(private router:Router) {}

  onClickboton(){
   
    this.router.navigateByUrl('tabs');
     if(this.textUsername==""){
      this.colore="danger";
     }else{
      this.colore="";
  
     }
     if(this.textPass==""){
      this.colore2="danger";
     }else{
      this.colore2="";
  
     }
  
   }
}
