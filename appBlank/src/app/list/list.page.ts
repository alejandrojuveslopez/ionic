import { AnimalsService } from './../animals.service';
import { TabsPage } from './../../../../myFirstionicApp/src/app/tabs/tabs.page';
import { Router, NavigationExtras, Routes } from '@angular/router';
import { Animal } from '../model/animalinterface';
import { ANIMALES } from './../data/data.animales';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
  
})

export class ListPage implements OnInit {
  animales:Animal[]=[];
  textsonido:string = "off"
  

  constructor(private router:Router, private animalService:AnimalsService) {

    this.animales=ANIMALES.slice(0);
    
    
  }

  onClickanimal(juanra){
    console.log(juanra);
  }

  onClickaudio(musica, juanaudio){
    let audio = new Audio();
    audio.src= musica;
    audio.load();
    audio.play();
    console.log("funciona")
    this.pausar_audio()
    setTimeout(function(){ this.textsonido="on"; }, juanaudio);
  }
  pausar_audio(){

  }
  onClickborrar(index:number){
    this.animales.splice(index,1);
    
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.animales=ANIMALES.slice(0);

      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
  
  /*toggleReorder() {
    const reorderGroup = document.getElementById('reorder');
    reorderGroup.disabled = !reorderGroup.disabled;
    reorderGroup.addEventListener('ionItemReorder', ({detail}) => {detail.complete(true);});
    
  } */
  ngOnInit(){
  }

  onClickboton(){
   
    this.router.navigateByUrl('slide');
  }
  
  onClickeditar(){
   
    this.router.navigateByUrl('edit');
  }
  
  onClickadd(){
    this.router.navigateByUrl('add');

  }
}
