import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
  
      children: [
        {
          path: 'list',
          children: [
            {
              path: '',
              loadChildren: () => import('../list/list.module').then( m => m.ListPageModule)
            }
          ]
        },
        {

          path: 'slide',
          children: [
            {
              path: '',
              loadChildren: () => import('../slide/slide.module').then( m => m.SlidePageModule)
            }
          ]
        },
        {
          path: '',
          redirectTo: 'tabs/list',
          pathMatch: 'full'
        }
      ]
    },
    {
      path: '',
      redirectTo: 'tabs/slide',
      pathMatch: 'full'
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
