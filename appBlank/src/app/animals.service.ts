import { ANIMALES } from './data/data.animales';
import { Animal } from './model/animalinterface';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {
  animals:Animal[]=[];
  constructor() { }
  getAnimals(){
    return this.animals;
  }

  setAnimals(animals:Animal[]){
    this.animals=animals;
  }
}
