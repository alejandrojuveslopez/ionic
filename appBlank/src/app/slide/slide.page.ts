import { Router, NavigationExtras } from '@angular/router';
import { Animal } from '../model/animalinterface';
import { ANIMALES } from './../data/data.animales';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-slide',
  templateUrl: './slide.page.html',
  styleUrls: ['./slide.page.scss'],
})
export class SlidePage implements OnInit {
  animales:Animal[]=[];


  constructor(private router:Router) { 
    this.animales=ANIMALES.slice(0);

  }
  

  ngOnInit() {
  }
  onClickboton(){
   
    this.router.navigateByUrl('list');
  }
}
