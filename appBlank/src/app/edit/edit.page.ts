import { Animal } from './../model/animalinterface';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  animales:Animal;
  nameani:string="";
  constructor() { }

  ngOnInit() {
  }
  newname(){
    this.animales.nombre=this.nameani;
  }
}
